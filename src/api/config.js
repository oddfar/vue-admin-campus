import request from '@/utils/request'

const api_name = 'admin/config'

// 查询参数列表
export function listConfig(page, limit, query) {
    return request({
        url: `${api_name}/${page}/${limit}`,
        method: 'post',
        data: query
    })
}


// 查询参数详细
export function getConfig(configId) {
    return request({
        url: '/admin/config/' + configId,
        method: 'get'
    })
}

// 根据参数键名查询参数值
export function getConfigKey(configKey) {
    return request({
        url: '/system/config/configKey/' + configKey,
        method: 'get'
    })
}

// 新增参数配置
export function addConfig(data) {
    return request({
        url: '/admin/config',
        method: 'post',
        data: data
    })
}

// 修改参数配置
export function updateConfig(data) {
    return request({
        url: '/admin/config',
        method: 'put',
        data: data
    })
}

// 删除参数配置
export function delConfig(configId) {
    return request({
        url: '/admin/config/' + configId,
        method: 'delete',

    })
}

// 刷新参数缓存
export function refreshCache() {
    return request({
        url: `${api_name}/refreshCache`,
        method: 'delete'
    })
}
