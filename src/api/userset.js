import request from '@/utils/request'

const api_name = 'admin/user'

export default {
    //获取列表
    listUser(page, limit, searchObj) {
        return request({
            url: `${api_name}/${page}/${limit}`,
            method: 'post',
            data: searchObj
        })
    },
    //获取用户详情
    getUser(id) {
        return request({
            url: `${api_name}/getUserSet/${id}`,
            method: 'get'
        })
    },

    //删除字典
    delUser(userId) {
        return request({
            url: '/admin/user/' + userId,
            method: 'delete',
        })
    },
    //锁定和取消锁定
    changeUserStatus(id, status) {
        return request({
            url: `${api_name}/lock/${id}/${status}`,
            method: 'put'
        })
    },

    //修改
    updateUserSet(userSet) {
        return request({
            url: `${api_name}/updateUserSet`,
            method: 'post',
            data: userSet
        })
    },
    //添加
    saveUserSet(userSet) {
        return request({
            url: `${api_name}/add`,
            method: 'post',
            data: userSet
        })
    },

}
