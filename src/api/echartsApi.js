import request from '@/utils/request'

const api_name = 'admin/echarts'

export default {
    //查询文章最近7天有效数据的发表数量
    getContentsNum() {
        return request({
            url: `${api_name}/getContentsNum`,
            method: 'get',
        })
    },
    //查看每个分类的文章数
    getMetaContentNum() {
        return request({
            url: `${api_name}/getMetaContentNum`,
            method: 'get',
        })
    },
    //查看后台4个的总数
    getNums() {
        return request({
            url: `${api_name}/getNums`,
            method: 'get',
        })
    },





}
