import request from '@/utils/request'

const api_name = 'admin/dict'


export default {
    // 查询参数列表
    listConfig(page, limit, query) {
        return request({
            url: `${api_name}/${page}/${limit}`,
            method: 'post',
            data: query
        })
    },
    // 查询参数详细
    getDict(dictId) {
        return request({
            url: '/admin/dict/' + dictId,
            method: 'get'
        })
    },
    // 修改字典
    updateDict(dict) {
        return request({
            url: '/admin/dict',
            method: 'put',
            data: dict
        })
    },
    // 添加字典
    addDict(dict) {
        return request({
            url: '/admin/dict',
            method: 'post',
            data: dict
        })
    },
    //删除字典
    delDict(dictId) {
        return request({
            url: '/admin/dict/'+ dictId,
            method: 'delete',
        })
    },
}
