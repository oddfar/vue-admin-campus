import request from '@/utils/request'

const api_name = 'admin/meta'

export default {
    //获取列表
    getMetaSetList(page, limit, searchObj) {
        return request({
            url: `${api_name}/${page}/${limit}`,
            method: 'post',
            data: searchObj
        })
    },

    //获取所有列表
    getAll(){
        return request({
            url: `/tourist/getMetas`,
            method: 'get',
        })
    },
    

    //获取详情
    getMetaSet(id) {
        return request({
            url: `${api_name}/getMetaSet/${id}`,
            method: 'get'
        })
    },
    //删除
    deleteMetaSet(id) {
        return request({
            url: `${api_name}/delete`,
            method: 'post',
            data: id
        })
    },
    //批量删除
    removeRows(idList) {
        return request({
            url: `${api_name}/batchRemove`,
            method: 'delete',
            data: idList
        })
    },

    //修改
    updateMetaSet(metaSet) {
        return request({
            url: `${api_name}/edit`,
            method: 'post',
            data: metaSet
        })
    },
    //添加
    saveMetaSet(metaSet) {
        return request({
            url: `${api_name}/add`,
            method: 'post',
            data: metaSet
        })
    },

}
