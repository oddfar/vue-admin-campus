import request from '@/utils/request'

const api_name = 'admin/content'

export default {
    //获取列表
    getContentList(page, limit, searchObj) {
        return request({
            url: `${api_name}/${page}/${limit}`,
            method: 'post',
            data: searchObj
        })
    },
    //删除
    delete(id) {
        return request({
            url: `${api_name}/delete`,
            method: 'post',
            data: id
        })
    },
    //批量删除
    removeRows(idList) {
        return request({
            url: `${api_name}/batchRemove`,
            method: 'delete',
            data: idList
        })
    },
    //批量通过
    adoptRows(idList) {
        return request({
            url: `${api_name}/batchAdopt`,
            method: 'post',
            data: idList
        })
    },



    //获取详情
    getContentSet(id) {
        return request({
            url: `${api_name}/getContent/${id}`,
            method: 'get'
        })
    },

    // //修改
    edit(contentSet) {
        return request({
            url: `${api_name}/edit`,
            method: 'post',
            data: contentSet
        })
    },
    //添加
    add(contentSet) {
        return request({
            url: `${api_name}/add`,
            method: 'post',
            data: contentSet
        })
    },

}
