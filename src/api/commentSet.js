import request from '@/utils/request'

const api_name = 'admin/comment'

export default {
    //获取列表
    getList(page, limit, searchObj) {
        return request({
            url: `${api_name}/${page}/${limit}`,
            method: 'post',
            data: searchObj
        })
    },

    //删除
    delete(id) {
        return request({
            url: `${api_name}/delete`,
            method: 'post',
            data: id
        })
    },
    //批量删除
    removeRows(idList) {
        return request({
            url: `${api_name}/batchRemove`,
            method: 'delete',
            data: idList
        })
    },
}