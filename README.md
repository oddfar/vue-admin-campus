校园信息墙:后台管理前端




```bash
# 克隆项目
git clone https://gitee.com/oddfar/vue-admin-campus.git

# 进入项目目录
cd vue-admin-campus

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装以来，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```